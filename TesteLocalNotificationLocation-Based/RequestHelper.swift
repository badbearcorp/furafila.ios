//
//  RequestHelper.swift
//  APPPositivo
//
//  Created by Rodrigo Costa on 11/03/16.
//  Copyright © 2016 Rodrigo Costa. All rights reserved.
//

import UIKit

public class RequestHelper: NSObject {

    var response: NSURLResponse? = NSURLResponse()
    
    public func GET(url:NSURL!)->ResponseObj{
        let ret = ResponseObj()
        ret.responseCode = 404
        ret.dataArray = NSArray()
  //      CLSLogv("Log awesomeness %d %d %@", getVaList([1, 2, "three"]))
        
        let request = NSMutableURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "GET"
        NSLog(request.debugDescription)
        // send the request
        var data: NSData!
        do {
            data = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        } catch let e as NSError {
  //          Crashlytics.sharedInstance().recordError(e, withAdditionalUserInfo: ["request": request.debugDescription])
            return ret
        }
        if let httpResponse = response as? NSHTTPURLResponse {
            ret.responseCode = httpResponse.statusCode
            if(httpResponse.statusCode == 200){
                if data.length > 0 {
                    if let dataArray: NSArray = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSArray{
                        ret.dataArray = dataArray
                        return ret
                    }
                    else if let dataDict: NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary{
                        
                        ret.dataArray = NSArray(array: [dataDict])
                        return ret
                    }
                }else{
                    ret.responseCode = 202
                }
            }
        } else {
            NSLog("No HTTP response for request \(request.debugDescription)")
        }
        return ret
    }
    
}

public class ResponseObj{
    var responseCode:Int!
    var dataArray:NSArray!
}
