////
//  LoginViewController.swift
//  APPPositivo
//
//  Created by Rodrigo Costa on 11/06/15.
//  Copyright (c) 2015 Rodrigo Costa. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UIAlertViewDelegate {

    
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet weak var inputLogin: UITextField!
    @IBOutlet weak var inputSenha: UITextField!
    @IBOutlet var imgViewLogo: UIImageView!
    @IBOutlet var viewFaixa: UIView!
    @IBOutlet var viewFundoBaixo: UIView!
    @IBOutlet var viewFundoTopo: UIView!
    @IBOutlet var btEntrar: UIButton!
    var txtlogin:String = ""
    var txtsenha:String = ""
    var login:Login!
    let delegate = (UIApplication.sharedApplication().delegate) as! AppDelegate
    let path = NSBundle.mainBundle().pathForResource("ColorConfig", ofType:"plist")
    func  alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        //Reset().resetBase()
        //exit(0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.hidden = true;
        needsUpdate()
        
        
        login  = Login()
    }
    
    
    func needsUpdate() -> Bool{
        let infoDictionary = NSBundle.mainBundle().infoDictionary! as NSDictionary
        let appID = infoDictionary["CFBundleIdentifier"] as! NSString;
        let url = NSURL(string: "http://itunes.apple.com/lookup?bundleId=\(appID)")
        if let data = NSData(contentsOfURL: url!){
        do{
            let lookup = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
            
            if (lookup["resultCount"]!!.integerValue == 1){
                let appStoreVersion = ["results"][0]["version"] as! NSString
                let currentVersion = infoDictionary["CFBundleShortVersionString"] as! NSString;
                if (appStoreVersion != currentVersion){
                    UIAlertView(title: nil, message: NSLocalizedString("NOVA_VERSAO_DO_APP", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("FECHAR", comment: "")).show()
                    return true;
                }
            }
        }catch(let error){
            
        }
        }
        return false;
    }
    
    override func viewDidAppear(animated: Bool) {
        
        //login.sair()
       super.viewDidAppear(true)
        
        if login.estaLogado(){
            print("esta logado", terminator: "")
            proximaTela()
        }else{
            self.view.hidden = false
            print("nao esta logado", terminator: "")
        }
       
      //  carregaEstilos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func escondeTeclado(sender: UITextField) {
        self.resignFirstResponder()
    }

    
    
    @IBAction func entrar(sender: UIButton) {
        
        
       
        let  alert = UIAlertView(title: nil, message: "Por favor aguarde", delegate: self, cancelButtonTitle: nil)
        
        dispatch_async(dispatch_get_main_queue(), {
            self.indicator.hidden = false
            self.indicator.setNeedsDisplay()
            
            let spinner : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
            spinner.center = CGPointMake(alert.bounds.size.width / 2, alert.bounds.size.height / 2 )
            spinner.color = UIColor.blackColor()
            spinner.startAnimating()
            //   alert.addSubview(spinner)
            alert.show()
            dispatch_async(dispatch_get_main_queue(), {
                
                
                self.txtlogin = self.inputLogin.text!
                self.txtsenha = self.inputSenha.text!
                
              //  println(self.txtlogin)
              //  println(self.txtsenha)
                
                if self.login.logar(self.txtlogin, senha: self.txtsenha){
                    self.proximaTela()
                }else{
                    NSLog("falha", "Falha de login")
                    
                }
                dispatch_async(dispatch_get_main_queue(), {
                    self.indicator.hidden = true
                    alert.dismissWithClickedButtonIndex(0, animated: false)
                })
            })
        })
    }
    
    func proximaTela(){
        performSegueWithIdentifier("Principal", sender: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
      //  carregaEstilos()
    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}

