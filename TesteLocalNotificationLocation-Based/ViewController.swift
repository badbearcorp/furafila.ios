 //
//  ViewController.swift
//  TesteLocalNotificationLocation-Based
//
//  Created by Rodrigo Costa on 18/02/16.
//  Copyright © 2016 Universidade Positivo. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UIViewController,CLLocationManagerDelegate {

    
    @IBOutlet var indicador: UIActivityIndicatorView!
    let teste = CLLocation(latitude: -25.4219328, longitude: -49.2505034)
    let maxdistance = 600.0
    let maxPositionTime = 60.0
    @IBOutlet var botao: UIButton!
    
    var locationManager:CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        botao.layer.cornerRadius = botao.bounds.size.width / 8;
        
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func chamar(sender: AnyObject) {
        var mensagemRetorno = ""
        if(verifyTime(locationManager.location)){
            if(verifyDistance(locationManager.location!)){
                if(botao.titleLabel?.text == "Chamar"){
                    chamarFilhos()
                }else{
                    liberarFilhos()
                }
                return
            }
        }
        mensagemRetorno += "Não foi possível identificar sua localização"
        UIAlertView(title: nil, message: mensagemRetorno, delegate: nil, cancelButtonTitle: NSLocalizedString("FECHAR", comment: "")).show()
        disableButton()
        
    }
    
    func chamarFilhos(){
        let  alert = UIAlertView(title: nil, message: "Por favor aguarde", delegate: self, cancelButtonTitle: nil)
        
        dispatch_async(dispatch_get_main_queue(), {
            self.indicador.hidden = false
            self.indicador.setNeedsDisplay()
            
            let spinner : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
            spinner.center = CGPointMake(alert.bounds.size.width / 2, alert.bounds.size.height / 2 )
            spinner.color = UIColor.blackColor()
            spinner.startAnimating()
            //   alert.addSubview(spinner)
            alert.show()
            dispatch_async(dispatch_get_main_queue(), {
                var mensagemRetorno = ""
                let avatars = Avatar.getAll()
                for avatar in avatars!{
                    if(JsonManager.getInstance().chamar(avatar)){
                        mensagemRetorno +=  avatar.humanId+" chamado com sucesso! \n"
                        self.botao.setTitle("Liberar", forState: UIControlState.Normal)
                    }else{
                        mensagemRetorno +=  "Não foi possível chamar " + avatar.humanId+"\n"
                    }
                }
                UIAlertView(title: nil, message: mensagemRetorno, delegate: nil, cancelButtonTitle: NSLocalizedString("FECHAR", comment: "")).show()
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.indicador.hidden = true
                    alert.dismissWithClickedButtonIndex(0, animated: false)
                })
            })
        })
    }
    func liberarFilhos(){
        let  alert = UIAlertView(title: nil, message: "Por favor aguarde", delegate: self, cancelButtonTitle: nil)
        
        dispatch_async(dispatch_get_main_queue(), {
            self.indicador.hidden = false
            self.indicador.setNeedsDisplay()
            
            let spinner : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
            spinner.center = CGPointMake(alert.bounds.size.width / 2, alert.bounds.size.height / 2 )
            spinner.color = UIColor.blackColor()
            spinner.startAnimating()
            //   alert.addSubview(spinner)
            alert.show()
            dispatch_async(dispatch_get_main_queue(), {
                var mensagemRetorno = ""
                let avatars = Avatar.getAll()
                for avatar in avatars!{
                    if(JsonManager.getInstance().chamar(avatar)){
                        mensagemRetorno +=  avatar.humanId+" liberado com sucesso! \n"
                        self.botao.setTitle("Chamar", forState: UIControlState.Normal)
                    }else{
                        mensagemRetorno +=  "Não foi possível liberar " + avatar.humanId+"\n"
                    }
                }
                UIAlertView(title: nil, message: mensagemRetorno, delegate: nil, cancelButtonTitle: "Fechar").show()
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.indicador.hidden = true
                    alert.dismissWithClickedButtonIndex(0, animated: true)
                })
            })
        })
    }
    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.AuthorizedWhenInUse){
            startShowingLocationNotifications()
            locationManager.startUpdatingLocation()
        }
    }
    func disableButton(){
        botao.enabled = false
        botao.backgroundColor = UIColor(red: 220/256, green: 220/256, blue: 220/256, alpha: 1)
        botao.setTitleColor(UIColor(red: 175/256, green: 175/256, blue: 175/256, alpha: 1), forState: UIControlState.Normal)
        botao.setTitle("Bloqueado", forState: UIControlState.Normal)
    }
    
    func enableButton(){
        botao.enabled = true
        botao.backgroundColor = UIColor(red: 247/256, green: 148/256, blue: 30/256, alpha: 1)
        botao.setTitleColor(UIColor(red: 127/256, green: 56/256, blue: 13/256, alpha: 1), forState: UIControlState.Normal)
        if(botao.titleLabel!.text == "Bloqueado"){
                botao.setTitle("Chamar", forState: UIControlState.Normal)
        }
    }
    func verifyDistance(location:CLLocation)->Bool{
        if(location.distanceFromLocation(teste) <= maxdistance){
            return true
        }
        return false
    }
    func verifyTime(location:CLLocation?)->Bool{
        if (location != nil){
            if ((location!.timestamp.timeIntervalSinceNow as Double * -1) <= maxPositionTime){
                return true
            }
        }
        return false
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //(locations.debugDescription)
        if(verifyTime(locations.last)){
            if(verifyDistance(locations.last!)){
                enableButton()
                return;
            }
        }
        print(locations.last!.timestamp.timeIntervalSinceNow as Double)
        print(locations.last!.distanceFromLocation(teste))
        disableButton()
    }
   
 
    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager) {
        print("location manager is down")
    }
    
    func startShowingLocationNotifications() {
        let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: -25.4219328, longitude: -49.2505034), radius: 800.00, identifier: "area-de-liberacao")
        region.notifyOnExit = false
        region.notifyOnEntry = true
        
        let notification = UILocalNotification()
        notification.alertBody = "Chegou na area de liberação!"
        notification.regionTriggersOnce = false
        notification.region = region
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    
    }
}

