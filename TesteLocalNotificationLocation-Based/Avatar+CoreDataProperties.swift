//
//  Avatar+CoreDataProperties.swift
//  APPPositivo
//
//  Created by Rodrigo Costa on 02/03/16.
//  Copyright © 2016 Rodrigo Costa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Avatar {

    @NSManaged var caninicalId: NSNumber!
    @NSManaged var humanId: String!
    @NSManaged var isActive: NSNumber!
    @NSManaged var nickName: String!
    @NSManaged var serie: String!
    @NSManaged var uriid: String!

}
