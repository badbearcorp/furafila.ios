//
//  Login.swift
//  APPPositivo
//
//  Created by Rodrigo Costa on 15/06/15.
//  Copyright (c) 2015 Rodrigo Costa. All rights reserved.
//

import UIKit


public class Login: NSObject {
    
    let delegate = (UIApplication.sharedApplication().delegate) as! AppDelegate
    override init() {
        
        
        super.init()
    }

    
    public static func getIdPai()->String?{
        let preferences = NSUserDefaults.standardUserDefaults()
        let idKey = "idPai"
        
        if (preferences.objectForKey(idKey) == nil)
        {
            return nil
        }
        else
        {
            //  Get current level
            return preferences.stringForKey(idKey)!
        }
    }
    public static func setID(id:String){
        let preferences = NSUserDefaults.standardUserDefaults()
        let idKey = "idPai"
        
        preferences.setObject(id, forKey: idKey)
        
        //  Save to disk
        let didSave = preferences.synchronize()
        
        if (!didSave)
        {
            //  Couldn't save (I've never seen this happen in real world testing)
        }
    }
    
    public func estaLogado()->Bool{
        if let id = Login.getIdPai(){
            return true
        }
        return false
    }
    
    public func logar(login:String, senha:String)-> Bool{
        
        let json:JsonManager = JsonManager.getInstance()
        let idPai:NSString = json.login(login, senha: senha)//"WumIVenH"/////////////////////////////////TESTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //idPai = "GGEBVhWJ"/////////////////////////////////TESTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        if idPai.length > 0 {
            Login.setID(idPai as! String)
            if( json.arrayFilhos().count > 0 ) {
                return true
            }
        }
        NSLog("falha", "json retornou em branco")
        return false
    }
    
       
}
