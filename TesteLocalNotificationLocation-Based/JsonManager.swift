//
//  JsonManager.swift
//  APPPositivo
//
//  Created by Rodrigo Costa on 18/06/15.
//  Copyright (c) 2015 Rodrigo Costa. All rights reserved.
//

import UIKit
import Darwin

public class JsonManager: NSObject , UIAlertViewDelegate {
    
    var response: NSURLResponse? = NSURLResponse()
    var error: NSError?
    var delegate : AppDelegate!
    private static var instance: JsonManager? = nil
    private override init() {
        delegate = (UIApplication.sharedApplication().delegate) as! AppDelegate
        super.init()
    }
    
    public static func getInstance()->JsonManager!{
        if JsonManager.instance == nil{
            JsonManager.instance = JsonManager()
        }
        return instance!
    }
    
    public func requisitar(url: NSString, jsonRequest: NSString)->NSData{
        return NSData()
    }
    
    public func login(login:NSString, senha:NSString)->String{
        var msgFalhaLogin = ""
        let url = delegate.urlServidor + "/api/Users"
        let request = NSMutableURLRequest(URL: NSURL(string: url)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 20)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        // let jsonString1 = "{\"login\":\"86912925987\",\"pwd\":\"homolog\",\"gcm\":\"\(delegate.deviceTokenApp as! String)\", \"device\":\"ios\" }"
            
        let jsonString = "{\"login\":\"\(login)\",\"pwd\":\"\(senha)\",\"gcm\":\"catraca\", \"device\":\"ios\" }"
        NSLog(jsonString)
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "POST"
        // send the request
        var data: NSData?
        do {
            data = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        } catch let error1 as NSError {
            error = error1
            data = nil
        }
        // look at the response
        if let httpResponse = response as? NSHTTPURLResponse {
            NSLog("RESPOSTA LOGIN ==> \(httpResponse.statusCode)")
            if httpResponse.statusCode == 200{
                var responseStr:NSString = NSString(data:data!, encoding:NSUTF8StringEncoding)!
                if data != nil{
                    if data!.length > 0{
                        let dataDict: NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                        //  NSLog(responseStr.debugDescription)
                        NSLog(dataDict.debugDescription)
                        //delegate.idCanonicoPai = dataDict.valueForKey("canonicalId") as! NSString
                        return dataDict.valueForKey("id") as! String
                        
                    }
                }
            }else if(httpResponse.statusCode == 501){
                msgFalhaLogin = NSLocalizedString("LOGIN_SEM_FILHO", comment: "")
              //  CLSLogv("Log LOGIN SEM FILHO login='%@'", getVaList([login]))
              //  Crashlytics.sharedInstance().crash()
                
            }else if(httpResponse.statusCode == 401){//não funciona em requisição sincrona
              //  msgFalhaLogin = NSLocalizedString("SENHA_INVALIDA", comment: "")
                
                
            }else if(httpResponse.statusCode == 404){
                msgFalhaLogin = NSLocalizedString("USUARIO_INVALIDO", comment: "")
                
                
            }else if(httpResponse.statusCode == 403){
                msgFalhaLogin = NSLocalizedString("USUARIO_BLOQUEADO", comment: "")
             //   CLSLogv("Log LOGIN USUARIO '%@' BLOQUEADO", getVaList([login]))
            }
            UIAlertView(title: NSLocalizedString("FALHA_LOGIN", comment: ""), message: msgFalhaLogin, delegate: self, cancelButtonTitle: NSLocalizedString("FECHAR", comment: "")).show()
            return ""
        }else if((error?.debugDescription.rangeOfString("-1012")) != nil){//substitui o erro 401
            
            msgFalhaLogin = NSLocalizedString("SENHA_INVALIDA", comment: "")
            UIAlertView(title: NSLocalizedString("FALHA_LOGIN", comment: ""), message: msgFalhaLogin, delegate: self, cancelButtonTitle: NSLocalizedString("FECHAR", comment: "")).show()
        }else {
            NSLog("No HTTP response")
            //NSLog(error?.debugDescription)
            
            UIAlertView(title: NSLocalizedString("FALHA_LOGIN", comment: ""), message: NSLocalizedString("FALHA_LOGIN_MSG", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("FECHAR", comment: "")).show()
            return ""
        }
        
       // NSLog(error!.debugDescription)
        
        return ""
        
    }
    
    public func liberar(avatar:Avatar)->Bool{
        let strurl = delegate.urlServidorLiberacao + "/api/Liberar/matricula=" + avatar.caninicalId.stringValue
        let url = NSURL(string: strurl)!
        return RequestHelper().GET(url).responseCode == 200
    }
    
    public func chamar(avatar:Avatar)->Bool{
        
        let strurl = delegate.urlServidorLiberacao + "/api/Aviso/matricula=" + avatar.caninicalId.stringValue
        let url = NSURL(string: strurl)!
        return RequestHelper().GET(url).responseCode == 200
    }
    
    public func arrayFilhos()->NSArray{
        let strurl = delegate.urlServidor + "/api/User/\(Login.getIdPai()!)/Avatar"
        let request = NSMutableURLRequest(URL: NSURL(string:  strurl)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "GET"
        NSLog(request.debugDescription)
        // send the request
        var data: NSData!
        do {
            data = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        } catch let error1 as NSError {
            error = error1
            data = nil
        }
        if let httpResponse = response as? NSHTTPURLResponse {
            NSLog("HTTP response: \(httpResponse.statusCode)")
        }
        if data.length > 0{
            if let dataArray: NSArray = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? NSArray{
                // look at the response
                if let httpResponse = response as? NSHTTPURLResponse {
                    NSLog("HTTP response: \(httpResponse.statusCode)")
                    var responseStr:NSString = NSString(data:data, encoding:NSUTF8StringEncoding)!
                    for aux: AnyObject in dataArray{
                        let dict: NSDictionary = aux as! NSDictionary
                        let CanonicalId = dict.valueForKey("CanonicalId") as! NSNumber
                        let HumanId = dict.valueForKey("HumanId") as! String
                        let Serie = dict.valueForKey("Serie") as! String
                        let UriId = dict.valueForKey("UriId") as! String
                        let avatar = Avatar.create()
                        avatar.caninicalId = CanonicalId
                        avatar.humanId = HumanId
                        avatar.serie = Serie
                        avatar.uriid = UriId
                        avatar.saveChanges()
                        // NSLog(dict.debugDescription)
                    }
                    
                    return dataArray
                } else {
                    NSLog("No HTTP response")
                    print(error?.debugDescription)
                }
            }
        }
        print(error?.debugDescription)
        return NSArray()
    }

    
    
    
    
   
    public func scapeQuote(entrada:NSString)->NSString{
        return entrada.stringByReplacingOccurrencesOfString("'", withString: "''")
    }
    
    public func base64DecodeString(encodedString:String)-> NSString!{
        if let decodedData = NSData(base64EncodedString: encodedString, options: NSDataBase64DecodingOptions(rawValue: 0)){
            return  NSString(data: decodedData, encoding: NSUTF8StringEncoding)
        }
        return encodedString
    }
}
