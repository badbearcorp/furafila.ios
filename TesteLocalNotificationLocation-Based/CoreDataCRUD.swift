//
//  CoreDataCRUD.swift
//  APPPositivo
//
//  Created by Rodrigo Costa on 02/03/16.
//  Copyright © 2016 Rodrigo Costa. All rights reserved.
//
import Foundation
import CoreData

protocol CoreDataCRUD {
    
}

extension CoreDataCRUD where Self : NSManagedObject {
    static func create() -> Self {
        return NSEntityDescription.insertNewObjectForEntityForName(Utility.classNameAsString(self), inManagedObjectContext: AppDelegate.getDelegate().managedObjectContext! ) as! Self
    }
    
    static func findById(id: String) throws -> Self? {
        let fetchRequest = NSFetchRequest(entityName: Utility.classNameAsString(self))
        let predicate = NSPredicate(format: "id == %@", id)
        fetchRequest.predicate = predicate
        if let fetchResults = try AppDelegate.getDelegate().managedObjectContext!.executeFetchRequest(fetchRequest) as? [Self] {
            if fetchResults.count > 0 {
                return fetchResults[0]
            }
        }
        return nil
    }
    
    func delete() throws {
        
        // (self as NSManagedObject).removeEntity()
    }
    
    func saveChanges(){
        do {
            try AppDelegate.getDelegate().managedObjectContext!.save()
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    static func getAll()->[Self]?{
        /////
        let appDelegate = AppDelegate.getDelegate()
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: Utility.classNameAsString(self))
        
        //3
        do {
            let results =
            try managedContext!.executeFetchRequest(fetchRequest)
            return results as! [Self]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return nil
        /////
    }
    
    
}

class Utility{
    class func classNameAsString(obj: Any) -> String {
        return _stdlib_getDemangledTypeName(obj).componentsSeparatedByString(".")[1]
    }
}
